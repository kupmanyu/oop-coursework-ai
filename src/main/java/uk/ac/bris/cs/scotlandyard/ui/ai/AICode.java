/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.bris.cs.scotlandyard.ui.ai;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

import uk.ac.bris.cs.scotlandyard.model.Colour;
import static uk.ac.bris.cs.scotlandyard.model.Colour.BLACK;
import uk.ac.bris.cs.scotlandyard.model.Move;
import uk.ac.bris.cs.scotlandyard.model.TicketMove;
import uk.ac.bris.cs.scotlandyard.model.DoubleMove;
import uk.ac.bris.cs.scotlandyard.model.Transport;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYardView;
import uk.ac.bris.cs.gamekit.graph.Edge;
import uk.ac.bris.cs.scotlandyard.model.Ticket;
import static uk.ac.bris.cs.scotlandyard.model.Ticket.DOUBLE;
import static uk.ac.bris.cs.scotlandyard.model.Ticket.SECRET;
import static java.util.Objects.requireNonNull;

public class AICode {
    private final TreeNode<Integer> root = new TreeNode<>(0);      
    private final int max = Integer.MAX_VALUE;
    private final Map<Colour, Map<Integer, Integer>> shortestPath = new HashMap<>();
    public Move selectedMove;
    private int mrXlocation;
    //private Map<Integer, Set<Ticket>> transport = new HashMap<>();
    
    public AICode(ScotlandYardView view, int location) {
        //view.getGraph().getNodes().forEach(node -> moves.put(node.value(), new ArrayList<>()));
        Map <Integer, Integer> temp = new HashMap<>();        
        view.getGraph().getNodes().forEach(node -> temp.put(node.value(), 0));
        view.getPlayers().forEach(colour -> shortestPath.put(colour, temp));
        //Set<Ticket> secret = new HashSet<>();
        //secret.add(SECRET);
        //view.getGraph().getNodes().forEach(node -> transport.put(node.value(), secret));
        mrXlocation = location;
        selectedMove = selectMove(view, mrXlocation);
    }
    
    private Set<Integer> validLocation(ScotlandYardView view, int location, Colour colour) {
        Set<Integer> locations = new HashSet<>();
        Set<Ticket> tickets = new HashSet<>();
        List<Edge<Integer, Transport>> possibleEdges = new ArrayList<>();
        possibleEdges.addAll(view.getGraph().getEdgesFrom(view.getGraph().getNode(location)));
        for (Edge<Integer, Transport> possibleEdge : possibleEdges) {
            if (view.getPlayerTickets(colour, Ticket.fromTransport(possibleEdge.data())).get() != 0) {
                locations.add(possibleEdge.destination().value());                                
            }            
        }       
        List<Colour> players = view.getPlayers();
        for (Colour player : players) {
            if (locations.contains(view.getPlayerLocation(player).get()) && player != BLACK) {
                locations.remove(view.getPlayerLocation(player).get());            
            }
        }        
        return locations;
    }
    
    private void possibleNextMoveTree(ScotlandYardView view, int location, Set<Integer> locations) {
        root.setData(location);
        locations.forEach(l -> root.addChild(l));
        for (TreeNode<Integer> node : root.getChildren()) {
            Set<Integer> temp = validLocation(view, node.getData(), BLACK);
            temp.forEach(t -> node.addChild(t));
        }
    }
    
    private void findMinDistance(ScotlandYardView view, Colour colour) {
       //int steps = 0;
       int location = view.getPlayerLocation(colour).get();
       Map<Integer, Integer> nodeDistance = new HashMap<>();
       view.getGraph().getNodes().forEach(node -> nodeDistance.put(node.value(), max));      
       nodeDistance.replace(location, 0);
       Set<Integer> visited = new HashSet<>();
       Set<Integer> unvisited = new HashSet<>();
       unvisited.add(location);
       while (!unvisited.isEmpty()) {
           int currentLocation = getLowestDistance(unvisited, nodeDistance);
           Set<Integer> possibleDestinations = validLocation(view, currentLocation, colour);
           unvisited.remove(currentLocation);
           for (Integer possibleDestination : possibleDestinations) {
               if (!visited.contains(possibleDestination)) {
                   calculateDistance(currentLocation, possibleDestination, nodeDistance, colour);
                   unvisited.add(possibleDestination);
               }
           }
           visited.add(currentLocation);
           //if (visited.contains(destination))               
           //    unvisited.clear();           
       }       
    }
    
    private Integer getLowestDistance(Set<Integer> unvisited, Map<Integer, Integer> nodeDistance) {
        int lowestDistanceLocation = 0;
        int lowestDistance = max;
        for (Integer unvisitedNode : unvisited) {
            if (nodeDistance.get(unvisitedNode) < lowestDistance) {
                lowestDistance = nodeDistance.get(unvisitedNode);
                lowestDistanceLocation = unvisitedNode;
            }                
        }
        return lowestDistanceLocation;
    }
    
    private void calculateDistance(Integer currentLocation, Integer destination, Map<Integer, Integer> nodeDistance, Colour colour) {
        if (nodeDistance.get(currentLocation) + 1 < nodeDistance.get(destination)) {
            nodeDistance.replace(destination, nodeDistance.get(currentLocation) + 1);
            Integer shortestPathSteps = shortestPath.get(colour).get(currentLocation);
            shortestPathSteps++;
            shortestPath.get(colour).replace(destination, shortestPathSteps);
        }
    }
    
    private Move selectMove(ScotlandYardView view, int location) {
        DoubleMove dmove;
        TicketMove move;
        Set<Integer> locations = validLocation(view, location, BLACK);
        possibleNextMoveTree(view, location, locations);
        for (Colour player : view.getPlayers())
            if (player != BLACK)
                findMinDistance(view, player);
        if (closestDetective(view, location) < 3 && view.getRounds().size() - view.getCurrentRound() >= 2 && view.getPlayerTickets(BLACK, DOUBLE).get() != 0) {
            dmove = bestDoubleMoveLocation(view, location);
            mrXlocation = dmove.finalDestination();
            return dmove;
        }            
        else if (view.getRounds().size() - view.getCurrentRound() < 5 && view.getPlayerTickets(BLACK, DOUBLE).get() > 0) {
            dmove = bestDoubleMoveLocation(view, location);
            mrXlocation = dmove.finalDestination();
            return dmove;
        }
        else {
            move = bestMoveLocation(view, location);
            mrXlocation = move.destination();
            return move;
        }
    }
    
    private int closestDetective(ScotlandYardView view, int location) {
        int lowest = max;
        for (Colour player : view.getPlayers())
            if (player != BLACK)
                if (shortestPath.get(player).get(location) < lowest)
                    lowest = shortestPath.get(player).get(location);
        return lowest;
    }
    
    private DoubleMove bestDoubleMoveLocation(ScotlandYardView view, int location) {
        TreeNode<Integer> destination = root;
        double minAverageMoves = 0;
        double average = 0;
        Ticket firstT = null;
        Ticket secondT = null;
        for (Colour colour : view.getPlayers()) {
            if (colour != BLACK)
                findMinDistance(view, colour);
        }
        for (TreeNode<Integer> parentnode : root.getChildren()) {
            average = 0;
            for (TreeNode<Integer> node : parentnode.getChildren()) {
                for (Colour player : view.getPlayers()) {                 
                    if (player != BLACK) {
                        if (!shortestPath.get(player).get(node.getData()).equals(0))
                            average += shortestPath.get(player).get(node.getData());    
                    }
                }
                average = average / (view.getPlayers().size() - 1);
                if (average > minAverageMoves) {
                    minAverageMoves = average;
                    destination = node;
                }
                else if (average == minAverageMoves) {
                    if (validLocation(view, node.getData(), BLACK).size() > validLocation(view, destination.getData(), BLACK).size())
                        destination = node;
                }
            }
        }
        Set<Ticket> ftransport = getTransport(view, location, destination.getParent().getData());
        Set<Ticket> stransport = getTransport(view, destination.getParent().getData(), destination.getData());
        if (closestDetective(view, location) <= 2 && view.getPlayerTickets(BLACK, SECRET).get() >= 2) {
            firstT = SECRET;
            secondT = SECRET;
        }
        else if (closestDetective(view, location) <= 2 && view.getPlayerTickets(BLACK, SECRET).get() == 1) {
            if (ftransport.size() > stransport.size()) {
                firstT = SECRET;
                for (Ticket t : stransport) {
                    if (secondT == null)
                        secondT = t;
                    else {
                        if (view.getPlayerTickets(BLACK, t).get() > view.getPlayerTickets(BLACK, secondT).get())
                            secondT = t;
                    }
                }
            }   
            else {
                secondT = SECRET;
                for (Ticket t : ftransport) {
                    if (firstT == null)
                        firstT = t;
                    else {
                        if (view.getPlayerTickets(BLACK, t).get() > view.getPlayerTickets(BLACK, firstT).get())
                            firstT = t;
                    }
                }
            }
        }
        else {
            for (Ticket t : ftransport) {
                    if (firstT == null)
                        firstT = t;
                    else {
                        if (view.getPlayerTickets(BLACK, t).get() > view.getPlayerTickets(BLACK, firstT).get())
                            firstT = t;
                    }
                }
            for (Ticket t : stransport) {
                    if (t == firstT && secondT != null) {                        
                        if (view.getPlayerTickets(BLACK, t).get() - 1 > view.getPlayerTickets(BLACK, secondT).get())
                            secondT = t;
                    }
                    else {
                        if (secondT == null)
                            secondT = t;
                        else {
                            if (view.getPlayerTickets(BLACK, t).get() - 1 > view.getPlayerTickets(BLACK, secondT).get())
                                secondT = t;
                        }
                    }
                }
        }        
        TicketMove first = new TicketMove(BLACK, firstT, destination.getParent().getData());
        TicketMove second = new TicketMove(BLACK, secondT, destination.getData());
        DoubleMove move = new DoubleMove(BLACK, first, second);
        return move;
    }
    
    private TicketMove bestMoveLocation(ScotlandYardView view, int location) {
        TreeNode<Integer> destination = root;
        double minAverageMoves = 0;
        double average = 0;
        Ticket ticket = null;            
        for (Colour colour : view.getPlayers()) {
            if (colour != BLACK)
                findMinDistance(view, colour);            
        }
        for (TreeNode<Integer> node : root.getChildren()) {
            average = 0;
            for (Colour player : view.getPlayers()) {                 
                if (player != BLACK) {
                    if (!shortestPath.get(player).get(node.getData()).equals(0))
                        average += shortestPath.get(player).get(node.getData());                        
                }
            }
            average = average / (view.getPlayers().size() - 1);
            if (average > minAverageMoves) {
                minAverageMoves = average;
                destination = node;
            }
            else if (average == minAverageMoves) {
                if (validLocation(view, node.getData(), BLACK).size() > validLocation(view, destination.getData(), BLACK).size())
                    destination = node;
            }
        }
        Set<Ticket> transport = getTransport(view, location, destination.getData());
        if (closestDetective(view, location) < 3 && view.getPlayerTickets(BLACK, SECRET).get() != 0)
            ticket = SECRET;
        else {            
            for (Ticket t : transport) {
                if (ticket == null)
                    ticket = t;
                else {
                    if (view.getPlayerTickets(BLACK, t).get() > view.getPlayerTickets(BLACK, ticket).get())
                        ticket = t;
                }
            }
        }        
        TicketMove move = new TicketMove(BLACK, ticket, destination.getData());
        return move;
    }
    
    private Set<Ticket> getTransport(ScotlandYardView view, int location, int destination) {
        Set<Ticket> transport = new HashSet<>();
        List<Edge<Integer, Transport>> edges = new ArrayList<>();
        edges.addAll(view.getGraph().getEdgesFrom(view.getGraph().getNode(location)));
        List<Edge<Integer, Transport>> dedges = new ArrayList<>();
        for (Edge<Integer, Transport> edge : edges) {
            if (edge.destination().value() == destination) 
                dedges.add(edge);
        }
        for (Edge<Integer, Transport> dedge : dedges) 
            transport.add(Ticket.fromTransport(dedge.data()));
        return transport;
    }
}
